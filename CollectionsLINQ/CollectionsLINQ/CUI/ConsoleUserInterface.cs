﻿using System;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Collections.Generic;
using System.Text;
using System.Net;
using CollectionsLINQ.Models;
using CollectionsLINQ.Extensions;

namespace CollectionsLINQ.CUI
{
    internal class ConsoleUserInterface
    {
        private readonly ServiceCUI _serviceCUI;

        public ConsoleUserInterface()
        {
            _serviceCUI = new ServiceCUI();
        }

        public void Execute()
        {
            char key = '\0';
            while (key != 'q')
            {
                DisplayMainMenu();
                key = Console.ReadKey().KeyChar;
                Console.Clear();

                switch (key)
                {
                    case 'q':
                        return ;
                    case '1':
                        DisplayProjectsOfAuthor();
                        break;
                    case '2':
                        DisplayTasksOfPerformer();
                        break;
                    case '3':
                        DisplayPerformerCurrentYearFinishedTasks();
                        break;
                    case '4':
                        DisplayListOfTeams();
                        break;
                    case '5':
                        DisplayListOfUsersWithTasks();
                        break;
                    case '6':
                        DisplayUserTasksInfo();
                        break;
                    case '7':
                        DisplayProjectInfo();
                        break;
                }

                WaitResults();
            }
        }

        private void DisplayMainMenu()
        {
            Console.WriteLine("Choose the option: ");
            Console.WriteLine("[1] - Show the number of tasks in projects by the project author");
            Console.WriteLine("[2] - Show all tasks of performer");
            Console.WriteLine("[3] - Show all tasks of performer finished in current year");
            Console.WriteLine("[4] - Show teams where all users elder than 10 years");
            Console.WriteLine("[5] - Show all users with tasks");
            Console.WriteLine("[6] - Show user special information");
            Console.WriteLine("[7] - Put the Vehicle in the Parking\n");
            Console.WriteLine("[q] - Quit");
        }

        private void WaitResults()
        {
            Console.WriteLine("Wait for results and then press any key to back to menu");
            Console.ReadKey();
            Console.Clear();
        }

        private void InputId(out int numForInput)
        {
            while (true)
            {
                Console.WriteLine("Input ID:");

                bool isInputValid = int.TryParse(Console.ReadLine(), out numForInput);

                if (isInputValid)
                {
                    break;
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("Invalid input. Please try again");
                }
            }

            Console.Clear();
        }

        private async void DisplayProjectsOfAuthor()
        {
            int authorId;
            InputId(out authorId);

            var projectsDictionary = await _serviceCUI.ProjectsDictionary(authorId);

            if (projectsDictionary == default)
            {
                Console.WriteLine("Server error");
                return;
            }

            if (projectsDictionary.Count() == 0)
            {
                Console.WriteLine($"Author with ID {authorId} has no projects");
                return;
            }

            foreach (var pair in projectsDictionary)
            {
                string projectName = pair.Key.Name == null ? "Project " + pair.Key.Id : pair.Key.Name;
                Console.WriteLine($"{projectName} {pair.Value}");
            }
        }

        private async void DisplayTasksOfPerformer()
        {
            int performerId;
            InputId(out performerId);

            var performerTasks = await _serviceCUI.PerformerTasks(performerId);

            if (performerTasks == default)
            {
                Console.WriteLine("Server error");
                return;
            }

            if (performerTasks.Count() == 0)
            {
                Console.WriteLine($"Performer with ID {performerId} has no tasks");
                return;
            }               

            foreach (Task task in performerTasks)
            {
                string taskName = task.Name == null ? "Task " + task.Id : task.Name;
                Console.WriteLine(taskName);
            }     
        }

        private async void DisplayPerformerCurrentYearFinishedTasks()
        {
            int performerId;
            InputId(out performerId);

            var performerTasks = await _serviceCUI.PerformerCurrentYearFinishedTasks(performerId);

            if (performerTasks == default)
            {
                Console.WriteLine("Server error");
                return;
            }

            if (performerTasks.Count() == 0)
            {
                Console.WriteLine($"Performer with ID {performerId} has not finished tasks in current year");
                return;
            }

            Console.WriteLine("ID\tTask name");
            foreach (var task in performerTasks)
            {
                string taskName = task.Name == null ? "Task " + task.Id : task.Name;
                Console.WriteLine($"{task.Id}\t{taskName}");
            }
        }

        private async void DisplayListOfTeams()
        {
            var teamsUsers = await _serviceCUI.PerformerCurrentYearFinishedTasks();

            if (teamsUsers == default)
            {
                Console.WriteLine("Server error");
                return;
            }

            foreach (var teamUsers in teamsUsers)
            {
                Console.WriteLine($"{teamUsers.Id} {teamUsers.Name}");
                foreach (var user in teamUsers.Users)
                {
                    Console.WriteLine($"\t{user.FirstName} {user.LastName} {user.RegisteredAt}");
                }
                Console.WriteLine();
            }
        }

        private async void DisplayListOfUsersWithTasks()
        {
            var usersTasks = await _serviceCUI.UsersTasksList();

            if (usersTasks == default)
            {
                Console.WriteLine("Server error");
                return;
            }

            foreach (var userTasks in usersTasks)
            {
                Console.WriteLine($"{userTasks.User.FirstName} {userTasks.User.LastName}");
                foreach (var task in userTasks.Tasks)
                {
                    Console.WriteLine($"\t{task.Name}");
                }
                Console.WriteLine();
            }
        }

        private async void DisplayUserTasksInfo()
        {
            int userId;
            InputId(out userId);

            UserTasksRecord userTasks = null;
            try
            {
                userTasks = await _serviceCUI.UserTasksInfo(userId);
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
                return;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return;
            }

            if (userTasks == default)
            {
                Console.WriteLine("Server error");
                return;
            }

            Console.WriteLine($"{userTasks.User.FirstName} {userTasks.User.LastName} {userTasks.LastProject.Name} " +
                              $"{userTasks.ProjectTasks} {userTasks.NotFinishedTasks} {userTasks.LongestTask.Name}");
        }

        private async void DisplayProjectInfo()
        {
            int projectId;
            InputId(out projectId);

            ProjectRecord projectRecord = null;
            try
            {
                projectRecord = await _serviceCUI.ProjectInfo(projectId);
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
                return;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return;
            }

            if (projectRecord == default)
            {
                Console.WriteLine("Server error");
                return;
            }
            
            Console.WriteLine($"{projectRecord.Project.Name} \t {projectRecord.LongestTask.Description}\t" +
                              $"{projectRecord.ShortestTask.Name} {projectRecord.UsersCount}");
        }
    }
}
