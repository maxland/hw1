﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using CollectionsLINQ.Extensions;
using CollectionsLINQ.Models;
using Task = CollectionsLINQ.Models.Task;

namespace CollectionsLINQ.CUI
{
    internal class ServiceCUI
    {
        private readonly HttpClient _client;
        private const string DefaultUri = "https://bsa21.azurewebsites.net/";

        private readonly int _currentYear;

        public ServiceCUI()
        {
            _client = new HttpClient();
            _client.BaseAddress = new Uri(DefaultUri);
            _currentYear = DateTime.Now.Year;
        }

        private async Task<T> GetDeserializedResponse<T>(string requestUri)
        {
            var response = await _client.GetAsync(requestUri);

            if (!response.IsSuccessStatusCode)
            {
                return default(T);
            }

            string responseBody = await response.Content.ReadAsStringAsync();
            var deserializedResponse = JsonSerializer.Deserialize<T>(responseBody);

            return deserializedResponse;
        }

        public async Task<Dictionary<Project, int>> ProjectsDictionary(int authorId)
        {
            var projects = await GetDeserializedResponse<IEnumerable<Project>>("api/Projects");
            var tasks = await GetDeserializedResponse<IEnumerable<Task>>("api/Tasks");

            if (projects == default || tasks == default)
            {
                return default;
            }

            var authorProjects = projects
                                 .Where(p => p.AuthorId == authorId);

            if (authorProjects.Count() == 0)
            {
                return new Dictionary<Project, int>();
            }

            var projectsDictionary = authorProjects
                                     .GroupJoin(tasks,
                                                project => project.Id,
                                                task => task.ProjectId,
                                                (project, tasks) =>
                                                new
                                                {
                                                    Project = project,
                                                    TasksCount = tasks.Where(task => task.ProjectId == project.Id)
                                                                      .Count()
                                                })
                                     .ToDictionary(touple => touple.Project,
                                                   touple => touple.TasksCount);

            return projectsDictionary;
        }

        public async Task<IEnumerable<Task>> PerformerTasks(int performerId)
        {
            var tasks = await GetDeserializedResponse<IEnumerable<Task>>("api/Tasks");

            if (tasks == default)
            {
                return default;
            }

            int maxTaskNameLength = 45;
            var performerTasks = tasks
                                 .Where(tasks => tasks.PerformerId == performerId && tasks.Name != null &&
                                        tasks.Name?.Length < maxTaskNameLength);

            return performerTasks;
        }

        public async Task<IEnumerable<TaskRecord>> PerformerCurrentYearFinishedTasks(int performerId)
        {
            var tasks = await GetDeserializedResponse<IEnumerable<Task>>("api/Tasks");

            if (tasks == default)
            {
                return default;
            }

            var performerTasks = tasks
                                 .Where(task => task.PerformerId == performerId && task.FinishedAt != null &&
                                        task.FinishedAt?.Year == _currentYear)
                                 .Select(task => new TaskRecord(task.Id, task.Name));

            return performerTasks;
        }

        public async Task<IEnumerable<TeamRecord>> PerformerCurrentYearFinishedTasks()
        {
            var teams = await GetDeserializedResponse<IEnumerable<Team>>("api/Teams");
            var users = await GetDeserializedResponse<IEnumerable<User>>("api/Users");

            if (teams == default || users == default)
            {
                return default;
            }

            var teamsUsers = teams
                             .GroupJoin(users,
                                        team => team.Id,
                                        user => user.TeamId,
                                        (team, users) => 
                                        new TeamRecord(team.Id, team.Name,
                                                       users
                                                       .OrderByDescending(user => user.RegisteredAt)))
                             .Where(teamUsers => teamUsers.Users
                                                 .All(user => (_currentYear - user.BirthDay.Year) > 10));

            return teamsUsers;
        }

        public async Task<IEnumerable<UserRecord>> UsersTasksList()
        {
            var users = await GetDeserializedResponse<IEnumerable<User>>("api/Users");
            var tasks = await GetDeserializedResponse<IEnumerable<Task>>("api/Tasks");

            if (tasks == default || users == default)
            {
                return default;
            }

            var usersTasks = users
                                 .OrderBy(user => user.FirstName)
                                 .GroupJoin(tasks,
                                            user => user.Id,
                                            task => task.PerformerId,
                                            (user, tasks) =>
                                            new UserRecord(user, tasks
                                                                 .OrderByDescending(task => task.Name)));

            return usersTasks;
        }

        public async Task<UserTasksRecord> UserTasksInfo(int userId)
        {
            var user = await GetDeserializedResponse<User>("api/Users/" + userId);
            var projects = await GetDeserializedResponse<IEnumerable<Project>>("api/Projects");
            var tasks = await GetDeserializedResponse<IEnumerable<Task>>("api/Tasks");

            if (user == default || tasks == default || projects == default)
            {
                return default;
            }

            Project lastProject = projects
                                  .MaxBy(project => project.CreatedAt);

            int lastProjectTasksCount = tasks
                                        .Where(task => task.ProjectId == lastProject.Id)
                                        .Count();

            int userNotFinishedTasksCount = tasks
                                            .Where(task => task.PerformerId == userId && task.FinishedAt == null)
                                            .Count();

            Task userLongestTask = tasks
                                   .Where(task => task.FinishedAt != null)
                                   .MaxBy(task => task.FinishedAt.Value.Ticks - task.CreatedAt.Ticks);

            var userTasks = new UserTasksRecord(user, lastProject, lastProjectTasksCount, userNotFinishedTasksCount, userLongestTask);

            return userTasks;
        }

        public async Task<ProjectRecord> ProjectInfo(int projectId)
        {
            var project = await GetDeserializedResponse<Project>("api/Projects/" + projectId);
            var tasks = await GetDeserializedResponse<IEnumerable<Task>>("api/Tasks");
            var users = await GetDeserializedResponse<IEnumerable<User>>("api/Users");

            if (project == default || tasks == default || users == default)
            {
                return default;
            }

            var team = await GetDeserializedResponse<Team>("api/Teams/" + project.TeamId);
            if (team == default)
            {
                return default;
            }

            var projectTasks = tasks
                               .Where(task => task.ProjectId == projectId);

            Task longestTask = projectTasks
                               .MaxBy(task => task.Description.Length);

            Task shortestTasks = projectTasks
                                 .MinBy(task => task.Name.Length);

            int projectTeamUsersCount = 0;

            bool condition1 = project.Description.Length > 20;
            bool condition2 = projectTasks.Count() < 3;
            bool xorCondition = (condition1 || condition2) && (!condition1 || !condition2);
            if (xorCondition)
            {
                projectTeamUsersCount = users
                                        .Where(user => user.TeamId == project.TeamId)
                                        .Count();
            }

            ProjectRecord projectRecord = new ProjectRecord(project, longestTask, shortestTasks, projectTeamUsersCount);

            return projectRecord;
        }
    }
}
