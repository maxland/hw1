﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CollectionsLINQ.Extensions
{
    public static class LINQExtensions
    {
        public static T MaxBy<T, TKey>(this IEnumerable<T> collection, Func<T, TKey> selector) where TKey : IComparable<TKey>
        {
            if (collection == null)
                throw new ArgumentNullException("collection");

            if (collection.Count() == 0)
                throw new ArgumentException("Collection is empty.");

            T maxObj = collection.First();
            TKey maxKey = selector(maxObj);
            foreach (var item in collection)
            {
                TKey currentKey = selector(item);
                if (currentKey.CompareTo(maxKey) > 0)
                {
                    maxKey = currentKey;
                    maxObj = item;
                }
            }

            return maxObj;
        }

        public static T MinBy<T, TKey>(this IEnumerable<T> collection, Func<T, TKey> selector) where TKey : IComparable<TKey>
        {
            if (collection == null)
                throw new ArgumentNullException("collection");

            if (collection.Count() == 0)
                throw new ArgumentException("Collection is empty.");

            T minObj = collection.First();
            TKey minKey = selector(minObj);
            foreach (var item in collection)
            {
                TKey currentKey = selector(item);
                if (currentKey.CompareTo(minKey) < 0)
                {
                    minKey = currentKey;
                    minObj = item;
                }
            }

            return minObj;
        }
    }
}
