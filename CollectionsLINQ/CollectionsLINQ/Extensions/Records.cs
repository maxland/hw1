﻿using System.Collections.Generic;
using CollectionsLINQ.Models;

namespace CollectionsLINQ.Extensions
{
    public record TaskRecord(int Id, string Name);

    public record TeamRecord(int Id, string Name, IEnumerable<User> Users);

    public record UserRecord(User User, IEnumerable<Task> Tasks);

    public record UserTasksRecord(User User, Project LastProject, int ProjectTasks, int NotFinishedTasks, Task LongestTask);

    public record ProjectRecord(Project Project, Task LongestTask, Task ShortestTask, int UsersCount);
}
