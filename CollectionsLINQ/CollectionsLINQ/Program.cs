﻿using System;
using CollectionsLINQ.CUI;

namespace CollectionsLINQ
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleUserInterface cui = new ConsoleUserInterface();
            cui.Execute();
        }
    }
}
